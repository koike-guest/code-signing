#!/usr/bin/python3

# Copyright (C) 2017 Collabora Ltd
# 2017 Helen Koike <helen.koike@collabora.com>
#
# Ported from bash to python3 by Julien Cristau <jcristau@debian.org>
#
# Copyright (C) 2018 Dropbox, Inc.
# 2018 Luke Faraone <lfaraone@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

import argparse
import hashlib
import json
import os
import subprocess
import sys
import tempfile
import requests
import logging
import sqlalchemy
from requests_file import FileAdapter

from db import AuditLog, DBSession, PackageState


KEYRING_FILE='etc/external-signature-requests.kbx'

class Job(object):

    __slots__ = 'package', 'architecture', 'version', 'tempdir', 'archive', \
                'suite', 'template_unpack_dir', 'template_source_dir', \
                'bin_pkgs', 'changes_file_name',

    def __init__(self, package, architecture, suite, version, archive):
        self.package = package
        self.architecture = architecture
        self.tempdir = ""
        self.suite = suite
        self.archive = archive
        self.version = version
        self.template_unpack_dir = None
        self.template_source_dir = None
        self.bin_pkgs = []

config = {
    'commands': {
        # path to the sign-file command from Linux
        'linux_sign_file': '/usr/lib/linux-kbuild-4.9/scripts/sign-file',
        # path to our pesign wrapper script
        'sign-efi': '/usr/local/bin/pesign-wrap'
    },
    'efi': {
        # pkcs11 uri from `p11tool --list-token-urls`
        'pkcs11_uri': None,
        # path to the PEM or DER-format certificate
        'cert_path': None,
        # path to the nss store
        'certdir': '/srv/codesign/pki',
        # name of the token in the nss store
        'token': 'PIV_II (PIV Card Holder pin)',
        # name of the cert in the nss store
        'certname': 'Certificate for Digital Signature',
        'pin': None,
    },
    'archives': {
        "ftp-master": {
            "deb": [
                "https://deb.debian.org/debian",
                "https://incoming.debian.org/debian-buildd",
            ],
            "requests": "https://incoming.debian.org/debian-buildd/project/external-signatures/requests.json",  # + .gpg
        },
        "security-master": {
            "deb": [
                "https://security.debian.org/debian-security",
                "https://security-master.debian.org/debian-security-buildd"
            ],
            "requests": "https://security-master.debian.org/debian-security-buildd/project/external-signatures/requests.json",
        },
    },
    'maintainer': {
        'key_id': None,
    }
}
logging.basicConfig()
logger = logging.getLogger('signer')
logger.setLevel(logging.DEBUG)


def hash_file(f):
    m = hashlib.sha256()
    for c in iter(lambda : f.read(4096), b''):
        m.update(c)
    return m.hexdigest()

def sign_kmod(module_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    env['KBUILD_SIGN_PIN'] = config['efi']['pin']
    # use check_output instead of check_call as sign-file seems to send random
    # stuff to stderr even when it succeeds
    subprocess.check_output(
        [
            config['commands']['linux_sign_file'], '-d', 'sha256',
            config['efi']['pkcs11_uri'], config['efi']['cert_path'], module_path
        ],
        env=env,
        stderr=subprocess.STDOUT
    )

    os.rename(module_path + '.p7s', signature_path)

    with open(signature_path, 'rb') as f:
        fhash = hash_file(f)

    return fhash


def sign_efi(efi_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    env['PESIGN_PIN'] = config['efi']['pin']

    with open(signature_path, 'wb') as out:
        subprocess.check_call(
            [
                config['commands']['sign-efi'], config['efi']['certdir'],
                config['efi']['token'], config['efi']['certname'], efi_path
            ],
            env=env, stdout=out
        )

    with open(signature_path, 'rb') as f:
        return hash_file(f)


def create_chdist_if_not_exist(suite, archive):
    dist = "{}-{}".format(suite, archive)
    if dist not in subprocess.check_output(["chdist", "list"]).decode('ascii').strip():
        # TODO(koike): support more sections.
        # We're creating a chdist here, but we're going to nuke the sources list later, to support multiple sources
        logger.info("Creating new dist {}".format(dist))
        subprocess.check_call(["chdist", "create", dist, config['archives'][archive]['deb'][0], suite, "main"])

        with open(os.path.expanduser(os.path.join("~", ".chdist", dist, "etc", "apt", "sources.list")),
                  "w") as sourceslist:
            for target in config['archives'][archive]['deb']:
                sourceslist.write(
                    "deb [arch={arches}] {url} {suite} {sections}".format(
                        arches="amd64,arm64,i386",
                        url=target,
                        suite=suite,
                        sections="main",
                    )
                )
        subprocess.check_call(["chdist", "apt", dist, "update"])


def download_pkg(job, pkg):
    logger.info("Downloading: {}".format(pkg))
    subprocess.check_call([
        "chdist", "-a", job.architecture, "apt-get", "{}-{}".format(job.suite, job.archive),
        "download", "{bin}={ver}".format(
            bin=pkg,
            ver=job.version,
        )
    ], cwd=job.tempdir)

def build_pkg_full_name(job, pkg):
    return "{}_{}_{}".format(pkg, job.version, job.architecture)

def extract_pkg(job, pkg):
    pkg = build_pkg_full_name(job, pkg)
    unpack_dir = "/".join([job.tempdir, pkg])
    os.makedirs(unpack_dir)
    subprocess.check_call([
        "dpkg", "-x", pkg + ".deb", unpack_dir
    ], cwd=job.tempdir)
    return unpack_dir


def download_template(job):
    # TODO(lfaraone): need to create per-archive suites
    create_chdist_if_not_exist(job.suite, job.archive)
    download_pkg(job, job.package)


def extract_template(job):
    job.template_unpack_dir = extract_pkg(job, job.package)
    job.template_source_dir = os.path.join(job.template_unpack_dir, "usr", "share", "code-signing", job.package, "source-template")


def get_bin_pkg_list(job):
    bin_pkg_list = "{base_dir}/usr/share/code-signing/{pkg_name}/files.json".format(base_dir=job.template_unpack_dir,
                                                                                  pkg_name=job.package)
    with open(bin_pkg_list) as fp:
        job.bin_pkgs = json.load(fp)


# TODO: optimizations to not iterate over lists again and again
def download_n_extract_binaries(job):
    for pkg in job.bin_pkgs:
        download_pkg(job, pkg)
        extract_pkg(job, pkg)


def check_template(job):
    # Check if mandatory files exist
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'source', 'format')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'changelog')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'control')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'copyright')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'rules')))
    # Check content of debian/source/format
    with open(os.path.join(job.template_source_dir, 'debian', 'source', 'format'), 'r') as f:
        assert(f.read() == "3.0 (native)\n")
    # Check signatures folder doesn't exist
    assert(not os.path.exists(os.path.join(job.template_source_dir, 'debian', 'signatures')))
    # Check that files.json doesn't use absolute paths
    for pkg, metadata in job.bin_pkgs.items():
        for file in metadata['files']:
            assert(not os.path.isabs(file["file"]))


def log_presign(file_path, fhash, package, version):
    s = DBSession()
    entry = AuditLog(
        binpkg_name=package,
        binpkg_version=version,
        fhash=fhash,
        file_path=file_path,
    )
    s.add(entry)
    s.commit()
    return entry.id


def log_signature(file_path, shash, package, version, presign_id):
    s = DBSession()
    entry = AuditLog(
        binpkg_name=package,
        binpkg_version=version,
        shash=shash,
        file_path=file_path,
        presign_id=presign_id,
    )
    s.add(entry)
    s.commit()


def sign_n_log_files(job):
    for pkg, metadata in job.bin_pkgs.items():
        for file in metadata['files']:
            file_path = os.path.join(job.tempdir, build_pkg_full_name(job, pkg), file["file"])
            sig_path = os.path.join(job.template_source_dir, 'debian', 'signatures', pkg, file["file"] + ".sig")

            with open(file_path, 'rb') as f:
                fhash = hash_file(f)

            presign_id = log_presign(file_path, fhash, pkg, job.version)

            if not os.path.exists(os.path.dirname(sig_path)):
                os.makedirs(os.path.dirname(sig_path))
            if file["sig_type"] == "efi":
                sig_hash = sign_efi(file_path, sig_path)
            elif file["sig_type"] == "linux-module":
                sig_hash = sign_kmod(file_path, sig_path)
            else:
                raise ValueError("File Type Unknown")
            log_signature(file_path, sig_hash, pkg, job.version, presign_id)


def prepare_source(job):
    subprocess.check_call([
        "dpkg-source", "-b", "."
    ], cwd=job.template_source_dir)
    # ../linux-base_4.5_source.changes
    source_package_name = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Source"],
        cwd=job.template_source_dir).decode('ascii').strip()
    source_package_version = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Version"],
        cwd=job.template_source_dir).decode('ascii').strip()
    job.changes_file_name = "{}_{}_source.changes".format(source_package_name, source_package_version)
    subprocess.check_call([
        "dpkg-genchanges", "-S", "-D'Distribution: {}'".format(job.suite), "-O" +
        os.path.join("..", job.changes_file_name),
    ], cwd=job.template_source_dir)


def sign_source(job):
    logger.debug("About to sign dput {} to {}".format(job.archive, job.changes_file_name))
    subprocess.check_call(
        ['debsign', '-S', '-k', config['maintainer']['key_id']],
        cwd=job.template_source_dir)


def submit_source(job):
    subprocess.check_call(
        ['dput', job.archive, os.path.join("..", job.changes_file_name)],
        cwd=job.template_source_dir)


def validate_config():
    # Validate configuration
    return


def read_packages_list(url):
    s = requests.Session()
    s.mount('file://', FileAdapter())

    requests_json = s.get(url)
    requests_gpg = s.get(url + '.gpg')

    # TODO: See if there is a better way to write this
    with tempfile.TemporaryDirectory() as tempdir:
        requests_json_f = os.path.join(tempdir, 'requests.json')
        requests_gpg_f = requests_json_f + '.gpg'
        with open(requests_json_f, "wb") as f:
            f.write(requests_json.content)
        with open(requests_gpg_f, "wb") as f:
            f.write(requests_gpg.content)
        subprocess.check_call(['gpgv', '--keyring', KEYRING_FILE, requests_gpg_f, requests_json_f])

    return requests_json.json()


def filter_packages(pkgs):
    s = DBSession()
    skippers = s.query(
        PackageState.template_package_name, PackageState.template_package_version,
        PackageState.architecture
    ).filter(
        PackageState.template_package_name.in_([pkg['package'] for pkg in pkgs])
    ).having(
        sqlalchemy.or_(
            PackageState.state == 'submitted',
            sqlalchemy.and_(
                PackageState.state == 'failed',
                sqlalchemy.func.count(PackageState.state) >= 3
            )
        )
    ).group_by(
        PackageState.state
    ).all()

    return [pkg for pkg in pkgs if (
        pkg['package'], pkg['version'], pkg['architecture']) not in skippers]


def set_package_state(job: Job, state: str, error_msg=None):
    # TODO(lfaraone): state should be an enum
    s = DBSession()
    entry = PackageState(
        error_msg=error_msg,
        state=state,
        template_package_name=job.package,
        template_package_version=job.version,
        architecture=job.architecture
    )
    s.add(entry)
    s.commit()


def get_pending(archive, url):
    packages = read_packages_list(url)['packages']
    packages = filter_packages(packages)
    for pkg in packages:
        logger.debug("pkg %s", pkg)
        yield Job(package=pkg["package"],
                  architecture=pkg["architecture"],
                  suite=pkg["suite"],
                  version=pkg["version"],
                  archive=archive
        )


def process(job):
    with tempfile.TemporaryDirectory() as tempdir:
        job.tempdir = tempdir
        set_package_state(job, 'incomplete')
        download_template(job)
        extract_template(job)
        get_bin_pkg_list(job)
        check_template(job)
        download_n_extract_binaries(job)
        sign_n_log_files(job)
        prepare_source(job)
        sign_source(job)
        set_package_state(job, 'signed')
        submit_source(job)
        set_package_state(job, 'submitted')


def main():
    parser = argparse.ArgumentParser(
        description='sign files for secure boot'
    )
    parser.add_argument(
        '--config', '-c', type=str, default='/etc/codesign.json',
        help='configuration file')
    args = parser.parse_args()
    with open(args.config) as fp:
        cp = json.load(fp)
    config.update(cp)
    logger.debug("Configuration: {}".format(cp))
    validate_config()
    for archive, metadata in config['archives'].items():
        pending = get_pending(archive, metadata['requests'])
        logger.debug("Pending jobs from dak: {}".format(pending))
        for p in pending:
            try:
                process(p)
            except Exception as e:
                set_package_state(p, 'failed', str(e))
                logging.exception("")


if __name__ == '__main__':
    sys.exit(main())
